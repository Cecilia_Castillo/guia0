#include <iostream>
#include <list>
#include "Cadena.h"
#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
	private:
		string nombre;
		string id;
		list <Cadena> cadenas;
		
	public:
		Proteina(string nombre, string id);
		
		string get_nombre();
		string get_id();
		list <Cadena> get_cadenas();
		void set_nombre(string nombre);
		void set_id(string id);
		void set_add_cadenas(Cadena cadena);

};

#endif /*PROTEINA*/
