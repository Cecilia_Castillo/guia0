#include <iostream>
#include <list>
using namespace std;
#include "Proteina.h"
#include "Cadena.h"


Proteina::Proteina(std::string nombre, std::string id){
	this->nombre = nombre;
	this->id = id;
	
}

string Proteina::get_nombre(){
	return this->nombre;
}

string Proteina::get_id(){
	return this->id;
}

list <Cadena> Proteina::get_cadenas(){
	return this->cadenas;
}

void Proteina::set_nombre(string nombre){
	this->nombre = nombre;

}

void Proteina::set_id(string id){
	this->id = id;
}

void Proteina::set_add_cadenas(Cadena cadena){
	this->cadenas.push_back(cadena);
}
