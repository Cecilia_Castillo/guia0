#include <iostream>
#include <list>
#include "Proteina.h"
#ifndef PROGRAMA_H
#define PROGRAMA_H

//Este archivo es para definir la lista de proteinas

class Programa {
	private:
		list <Proteina> proteinas;
		
	public:
		Programa();
        list <Proteina> get_proteinas();
        void set_add_proteinas(Proteina proteina);

};

#endif /*PROTEINA*/
