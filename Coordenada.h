#ifndef COORDENADA_H_
#define COORDENADA_H_

class Coordenada {
    private:
        float x_coor = 0;
        float y_coor = 0;
        float z_coor = 0;

    public:
        /* constructores */
        Coordenada ();
        Coordenada (float x_coor, float y_coor, float z_coor);
        
         /* métodos get and set */
        float get_x_coor();
        float get_y_coor();
        float get_z_coor();
        void set_x_coor(float x_coor);
        void set_y_coor(float y_coor);
        void set_z_coor(float z_coor);
};
#endif /* COORDENADA_H_*/

