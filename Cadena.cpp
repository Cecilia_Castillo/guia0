#include <iostream>
#include <list>
using namespace std;
#include "Cadena.h"
#include "Amino.h"

Cadena::Cadena(std::string letra){
	this->letra = letra;
	
}

string Cadena::get_letra(){
	return this->letra;
}

list <Amino> Cadena::get_aminoacidos(){
	return this->aminoacidos;
}

void Cadena::set_letra(string letra){
	this->letra = letra;

}

void Cadena::set_add_aminoacidos(Amino amino){
	this->aminoacidos.push_back(amino);
}
