#include <iostream>
#include "Amino.h"
using namespace std;
#ifndef CADENA_H
#define CADENA_H

class Cadena {
	private:
		string letra;
		list <Amino> aminoacidos;
		
	public:
		Cadena(string letra);
		
		string get_letra();
        list <Amino> get_aminoacidos();
		void set_letra(string letra);
		void set_add_aminoacidos(Amino amino);

};

#endif /*CADENA*/
