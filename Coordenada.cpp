#include <iostream>
using namespace std;
#include "Coordenada.h"

/*Constructor*/
Coordenada::Coordenada() {
	float x_coor = 0;
	float y_coor = 0;
	float z_coor = 0;
}

Coordenada::Coordenada(float x_coor, float y_coor, float z_coor){
	this->x_coor = x_coor;
	this->y_coor = y_coor;
	this->z_coor = z_coor;
	
}

float Coordenada::get_x_coor(){
	return this->x_coor;
}

float Coordenada::get_y_coor(){
	return this->y_coor;
}

float Coordenada::get_z_coor(){
	return this->z_coor;
}

void Coordenada::set_x_coor(float x_coor){
	this->x_coor = x_coor;

}

void Coordenada::set_y_coor(float y_coor){
	this->y_coor = y_coor;
}

void Coordenada::set_z_coor(float z_coor){
	this->z_coor = z_coor;
}
