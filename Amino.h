#ifndef AMINO_H
#define AMINO_H
using namespace std;
#include <list>
#include <iostream>
#include "Atomo.h"

class Amino {
	private:
		string nombre;
		int numero;
		list <Atomo> atomos;
		
	public:
		Amino(std::string nombre, int numero);
		
		string get_nombre();
		int get_numero();
		list <Atomo> get_atomos();
		void set_nombre(string nombre);
		void set_numero(int numero);
		void set_add_atomos(Atomo atomo);

};

#endif /*AMINO*/
