#include <iostream>
#include <list>
using namespace std;
#include "Amino.h"
#include "Atomo.h"

Amino::Amino(std::string nombre, int numero){
	this->nombre = nombre;
	this->numero = numero;
	
}

string Amino::get_nombre(){
	return this->nombre;
}

int Amino::get_numero(){
	return this->numero;
}

list <Atomo> Amino::get_atomos(){
	return this->atomos;
}

void Amino::set_nombre(string nombre){
	this->nombre = nombre;

}

void Amino::set_numero(int numero){
	this->numero = numero;
}

void Amino::set_add_atomos(Atomo atomo){
	this->atomos.push_back(atomo);
}
